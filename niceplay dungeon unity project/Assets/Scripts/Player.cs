﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public static Player PlayerObject;
    
    private const float PLAYER_SPEED = 40;
    private const float SPELL_MAX_COOLDOWN = 0.2f;
    private float SpellCooldown;
    private bool Alive = true;
    
    // Start is called before the first frame update
    void Start()
    {
        PlayerObject = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (Alive)
        {
            // spell casting controls

            if (SpellCooldown > 0) SpellCooldown -= Time.deltaTime;

            if (SpellCooldown <= 0)
            {
                if (Input.GetKey(KeyCode.Mouse0)) // cast spell while button is hold
                {
                    SpellCooldown = SPELL_MAX_COOLDOWN;
                    
                    GameObject projectile = Instantiate(Resources.Load("Spell")) as GameObject;
                    projectile.transform.position = transform.position; // set position to position of player
                    projectile.GetComponent<Projectile>().Speed = 100;
                    projectile.GetComponent<Projectile>().Angle = Mathf.Atan2(Input.mousePosition.y - Screen.height / 2f, Input.mousePosition.x - Screen.width / 2f); // aim with a mouse from screen center
                }
            }
        }
    }

    private void FixedUpdate()
    {
        // moving controls
        if (Alive)
        {
            float moveX = 0;
            float moveY = 0;
            if (Input.GetKey(KeyCode.D)) moveX += 1;
            if (Input.GetKey(KeyCode.A)) moveX -= 1;
            if (Input.GetKey(KeyCode.W)) moveY += 1;
            if (Input.GetKey(KeyCode.S)) moveY -= 1;
            Vector2 newVelocity = new Vector2(moveX, moveY).normalized * PLAYER_SPEED;

            GetComponent<Rigidbody2D>().velocity = newVelocity;
        }
        
        // menu controls
        if (Input.GetKeyDown(KeyCode.R)) SceneManager.LoadScene("Level1");
        if (Input.GetKeyDown(KeyCode.Escape)) if (Application.platform != RuntimePlatform.WebGLPlayer) Application.Quit();
    }

    public void Victory()
    {
        transform.Find("VictoryText").gameObject.SetActive(true);
        StartCoroutine(RestartLevelWithDelay(3f));
    }
    
    public void Defeat()
    {
        transform.Find("DefeatText").gameObject.SetActive(true);

        // kill player
        {
            Alive = false;
            GetComponent<SpriteRenderer>().enabled = false; // hide sprite
            GetComponent<Collider2D>().enabled = false; // disable collider
            GetComponent<Rigidbody2D>().simulated = false; // disable rigidbody physics simulation
        }

        StartCoroutine(RestartLevelWithDelay(2f));
    }

    IEnumerator RestartLevelWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);

        SceneManager.LoadScene("Level1");
    }
}
